from tkinter import * 
import datetime
import math
import time

class Clock(Frame):
    def __init__(self, parent, **params):
        Frame.__init__(self, parent)
        
        self.sec_var = IntVar()
        self.sec_var.set(1)          

        self.can_width = 200      # sirka canvasu
        self.can_height = 200     # vyska canvasu
        self.line_width = 3       # sirka ciferniku
        self.hour_len = 0.7       # delka hodinove rucicky
        self.minute_len = 0.8     # delka minutove rucicky
        self.second_len = 0.9     # delka sekundove rucicky
        self.hour_width = 3       # sirka hodinove rucicky
        self.minute_width = 2     # sirka minutove rucicky
        self.second_width = 1     # sirka sekundove rucicky
        self.color = "black"

        if 'size' in params:
            self.can_width = int(params['size'])
            self.can_height = int(params['size'])          

        if 'color' in params:
            self.color = params['color']          

        self.c = Canvas(self, width=self.can_width, height=self.can_height)
       
        self.cc = self.c.create_oval(self.line_width, self.line_width, self.can_width-self.line_width, self.can_height-self.line_width, width=self.line_width, outline=self.color)
        self.hh = self.c.create_line(self.can_width/2, self.can_height/2, self.can_width/2, self.can_height/2 ,   width = self.hour_width, fill=self.color)
        self.mh = self.c.create_line(self.can_width/2, self.can_height/2, self.can_width/2, self.can_height/2 , width = self.minute_width, fill=self.color)
        self.sh = self.c.create_line(self.can_width/2, self.can_height/2, self.can_width/2, self.can_height/2 , width = self.second_width, fill=self.color)

        self.c.pack(expand = 1, fill=BOTH)
        self.c.bind("<Configure>", self.resize)

        self.timer_update()

    def timer_update(self):
        self.after(1000, self.timer_update)
        self.draw_current_time()

    def resize(self,event):
        #print('(%d, %d)' % (event.width, event.height))
        self.can_width=event.width
        self.can_height=event.height
        self.draw_current_time()
        
        
    def draw_current_time(self):
        dt = datetime.datetime.now()
                  
        xh = self.can_width/2 * self.hour_len * math.cos( float(dt.hour-3) * math.pi / 6.0)
        yh = self.can_height/2 * self.hour_len * math.sin( float(dt.hour-3) * math.pi / 6.0) 

        xm = self.can_width/2 * self.minute_len * math.cos( float(dt.minute-15) * math.pi / 30.0)
        ym = self.can_height/2 * self.minute_len * math.sin( float(dt.minute-15) * math.pi / 30.0)   

        xs = self.can_width/2 * self.second_len * math.cos(float(dt.second-15) * math.pi / 30.0)
        ys = self.can_height/2 * self.second_len * math.sin(float(dt.second-15) * math.pi / 30.0)      

        self.c.coords(self.cc, self.line_width, self.line_width, self.can_width-self.line_width, self.can_height-self.line_width)
        self.c.coords(self.hh, self.can_width/2, self.can_height/2, self.can_width/2 + xh, self.can_height/2 + yh)
        self.c.coords(self.mh, self.can_width/2, self.can_height/2, self.can_width/2 + xm, self.can_height/2 + ym)
        
        
        if(self.sec_var.get()): self.c.coords(self.sh, self.can_width/2, self.can_height/2, self.can_width/2 + xs, self.can_height/2 + ys)
        else: self.c.coords(self.sh, self.can_width/2, self.can_height/2, self.can_width/2, self.can_height/2)

    def set_alarm(self, time):
        h, m = time.split(":")
        t = (float(h)-3.0 + float(m)/60.0) * math.pi / 6.0
        x =  self.can_width/2 *  self.hour_len *  math.cos(t)
        y =  self.can_height/2 * self.hour_len *  math.sin(t)      
        self.ala = self.c.create_line(self.can_width/2, self.can_height/2, self.can_width/2 + x, self.can_height/2 + y, width = 2, fill="red")

    def reset_alarm(self):
        self.c.delete(self.ala)