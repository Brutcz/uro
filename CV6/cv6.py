from tkinter import * 
from tkinter.font import *
from Clock import Clock

class App(object):
    def __init__(self, root):
        self.root = root
        self.cl = Clock(self.root, size=250)
        self.cl.pack()

    def change_alpha(self, value):
        self.root.wm_attributes('-alpha', 1.0-float(value)/100.0)
    
    def set_alarm(self):
        pass

    def reset_alarm(self):
        pass

def main(): 
    root= Tk() 
    root.wm_title("Clock")
    root.tk_setPalette(background='white', foreground='black', activeBackground='white')
    root.resizable(False, False)
    app = App(root)
    root.mainloop() 

if __name__=='__main__': 
  main() 