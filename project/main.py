# -*- coding: utf-8 -*-

import json
import os
import tkinter as tk
import tkinter.font as tkf
import webbrowser
from os.path import normcase
from tkinter import *
from tkinter import messagebox
import tkinter.constants as tkc
from tkinter import ttk

from components import FilterList
from PIL import Image, ImageTk

hostings = {
    'google.cz': {
        'state': 'ok',
        'ip': '8.8.8.8',
        'https': True,
    },
    'xevos.cz': {
        'state': 'fail',
        'ip': '0.0.0.0',
        'https': False,
    },
    'seznam.cz': {
        'state': 'ok',
        'ip': '1.2.3.4',
        'https': True,
    },
}

contacts = {
    'michal.byrtus@vsb.cz': {
        'domains': ['google.cz', 'xevos.cz']
    }
}

base_path = os.path.dirname(os.path.realpath(__file__))
settings_path = os.path.normcase(os.path.join(base_path, 'settings.json'))


class App:

    def display_log(self, hosting):
        log_path = os.path.normcase(os.path.join(base_path, f"log/{hosting}.log"))

        self.uptime_log_text.configure(state=tkc.NORMAL)
        self.uptime_log_text.delete(1.0, tkc.END)

        if not os.path.exists(log_path):
            self.uptime_log_text.insert(tkc.END, f'Log file for {hosting} not exists!!!!')
        else:
            with open(log_path, 'r', encoding="utf-8") as f:
                for line in f:
                    self.uptime_log_text.insert(tkc.END, line)
        self.uptime_log_text.configure(state=tkc.DISABLED)

    def display_image(self, hosting):
        graph_img = ImageTk.PhotoImage(Image.open(os.path.normcase(os.path.join(base_path, f"{hosting}.graph.gif"))).resize((500, 500), Image.ANTIALIAS))
        self.graph_l.configure(image=graph_img)
        self.graph_l.image = graph_img

        if self.state.get():
            state_img = ImageTk.PhotoImage(Image.open(os.path.normcase(os.path.join(base_path, f"{self.state.get()}.png"))).resize((50, 50), Image.ANTIALIAS))
            self.host_state.configure(image=state_img)
            self.host_state.image = state_img

    def display(self, row):
        hosting = row[0]

        self.display_log(hosting)

        self.selected_hosting.set(hosting)

        for key in hostings[hosting].keys():
            var = getattr(self, key)
            if var is not None:
                var.set(hostings[hosting][key])

        protocol = 'https://' if self.https.get() else 'http://'
        self.url.set(f'{protocol}{hosting}')

        self.display_image(hosting)

    def setUpVariables(self):
        self.selected_hosting = StringVar()
        self.state = StringVar()
        self.ip = StringVar()
        self.url = StringVar()
        self.https = BooleanVar()

        self.selected_contact = StringVar()
        self.domains = StringVar()

    def loadSettings(self):
        with open(settings_path, 'r', encoding='utf8') as f:
            self.settings = json.load(f)

    def setUpSettingsVariables(self):
        self.loadSettings()

        types = {
            'int': IntVar,
            'str': StringVar,
            'bool': BooleanVar,
            'float': DoubleVar,
            'double': DoubleVar
        }

        for key in self.settings.keys():
            var_type = type(self.settings[key]).__name__
            if var_type in types:
                var = types[var_type](value=self.settings[key])

                setattr(self, key, var)

    def setUpMenu(self):
        root_menu = Menu(self.root)
        self.root.config(menu=root_menu)

        toolbar_menu = Menu(root_menu)
        root_menu.add_cascade(label="Akce", menu=toolbar_menu)
        toolbar_menu.add_command(label="Kontakty", command=self.display_contact_win)
        toolbar_menu.add_command(label="Nastavení", command=self.display_settings_win)
        toolbar_menu.add_separator()
        toolbar_menu.add_command(label="Konec", command=self._exit)

    def createFormRow(self, row, parent, text, variable_name, widget=Entry, bindings=[], **kwargs):
        if variable_name:
            kwargs['textvariable'] = getattr(self, variable_name)

        label = Label(parent, text=text)
        entry = widget(parent, **kwargs)

        if bindings:
            for binding in bindings:
                entry.bind(binding[0], binding[1])

        label.grid(column=1, row=row)
        entry.grid(column=3, row=row)

        setattr(self, f'{variable_name}_l', label)
        setattr(self, f'{variable_name}_e', entry)

    def open_link(self, url):
        webbrowser.open_new(url)

    def display_main_win(self):
        self.fr = Frame(self.root)
        self.fr.pack(expand=1, fill=tkc.BOTH)

        self.left_fr = Frame(self.root)
        self.left_fr.pack(expand=1, side=tkc.LEFT)

        self.fl = FilterList(self.left_fr, data=hostings, dataset=(('', 30), ('', 10)))
        self.fl.pack(expand=1, side=tkc.TOP)
        self.fl.subscribe(lambda row: self.display(self.fl.get_selected_row(row)))

        self.right_fr = Frame(self.root)
        self.right_fr.pack(expand=1, side=tkc.LEFT)

        self.host_info_label = Label(self.right_fr, textvariable=self.selected_hosting, font=self.heading_font)
        self.host_info_label.pack(expand=1)
        self.host_info = LabelFrame(self.right_fr, labelwidget=self.host_info_label, bd=1, relief=tkc.SUNKEN)
        self.host_info.pack(expand=1, fill=tkc.X)

        self.createFormRow(1, self.host_info, "URL", "url", Label, bindings=[("<Button-1>", lambda _: self.open_link(self.url.get()))], fg="blue", cursor="hand2")
        self.createFormRow(2, self.host_info, "IP", "ip", Label)

        self.host_state_l = Label(self.host_info, text="State")
        self.host_state_l.grid(column=1, row=3)
        self.host_state = Label(self.host_info)
        self.host_state.grid(column=3, row=3)

        self.configureGrid(self.host_info)

        self.graph = Frame(self.right_fr, bd=1, relief=tkc.SUNKEN)
        self.graph.pack(expand=1)

        self.graph_l = Label(self.graph)
        self.graph_l.pack(fill=tkc.BOTH, expand=1)

        self.uptime_log = Frame(self.right_fr, bd=1, relief=tkc.SUNKEN)
        self.uptime_log.pack(expand=1)

        self.uptime_log_text = Text(self.right_fr, wrap=tkc.WORD, width=45, height=20, state=tkc.DISABLED)
        self.uptime_log_text.pack(fill=tkc.X)

        # END OF FUNCTION
        self.fl.select_default()

    def display_contact(self, row):
        contact = contacts[row[0]]
        self.selected_contact.set(row[0])
        self.domains.set(contact['domains'])

    def getAllDomains(self):
        return list(hostings.keys())

    def saveContact(self):
        selected_domains = []

        for i in self.domains_e.curselection():
            selected_domains.append(self.domains_e.get(i))

        contacts[self.selected_contact.get()]['domains'] = ' '.join(selected_domains)
        index = self.contact_fl.curselection()
        self.contact_fl.update(index, (self.selected_contact.get(), contacts[self.selected_contact.get()]['domains']))

    def display_contact_win(self):
        win = Toplevel()
        win.title(f"{self.root.title()} - Kontakty")

        left_fr = Frame(win)
        left_fr.grid(column=0, row=0)

        self.contact_fl = FilterList(left_fr, data=contacts, dataset=(('', 30), ('', 30)), def_key='domains', def_val='')
        self.contact_fl.pack(expand=1, side=tkc.TOP)
        self.contact_fl.subscribe(lambda row: self.display_contact(self.contact_fl.get_selected_row(row)))
        self.contact_fl.select_default()

        right_fr = Frame(win)
        right_fr.grid(column=1, row=0)

        self.createFormRow(1, right_fr, 'Kontakt', 'selected_contact')

        self.domains_l = Label(right_fr, text='Domény')
        self.domains_l.grid(column=1, row=2)
        self.domains_e = Listbox(right_fr, selectmode=tkc.MULTIPLE)
        self.domains_e.grid(column=2, row=2, columnspan=3)

        for i, domain in enumerate(self.getAllDomains()):
            self.domains_e.insert(tkc.END, domain)
            if domain in self.domains.get():
                self.domains_e.select_set(i)
                self.domains_e.activate(i)

        save_btn = Button(right_fr, text='Uložit', command=self.saveContact)
        save_btn.grid(column=5, row=5)

        self.configureGrid(win)

        win.grab_set()

    def display_settings_win(self):
        win = Toplevel()
        win.title(f"{self.root.title()} - Nastavení")

        self.settings_f = LabelFrame(win, text="Nastavení")
        self.settings_f.pack(expand=1, fill=tkc.BOTH)

        self.createFormRow(1, self.settings_f, 'Velikost písma', 'font_size')
        self.createFormRow(2, self.settings_f, 'Písmo', 'font_family', ttk.Combobox, values=sorted(tkf.families()))
        self.createFormRow(3, self.settings_f, 'Váha písma', 'font_weight', ttk.Combobox, values=['normal', 'bold'])
        self.createFormRow(4, self.settings_f, 'Šířka obrazovky', 'window_width')
        self.createFormRow(5, self.settings_f, 'Výška obrazovky', 'window_height')
        self.createFormRow(6, self.settings_f, 'Odsazení okna zhora', 'window_top_offset')
        self.createFormRow(7, self.settings_f, 'Odsazení okna zprava', 'window_right_offset')

        self.save_settings_btn = Button(self.settings_f, text="Uložit", command=self.saveSettings)
        self.save_settings_btn.grid(column=5, row=9)

        self.configureGrid(self.settings_f)
        win.grab_set()

    def saveSettings(self):
        sett_dict = {}
        for key in self.settings.keys():
            sett_dict[key] = getattr(self, key).get()

        with open(os.path.normcase(os.path.join(base_path, 'settings.json')), 'w', encoding='utf8') as f:
            json.dump(sett_dict, f, indent=4)

        self.configureMainFont()
        self.configureWinSize()

    def configureGrid(self, element):
        col_count, row_count = element.grid_size()

        for col in range(col_count + 1):
            element.grid_columnconfigure(col, minsize=20, weight=1)

        for row in range(row_count + 1):
            element.grid_rowconfigure(row, minsize=20, weight=1)

    def configureMainFont(self):
        self.main_font.configure(family=self.font_family.get(), size=self.font_size.get(), weight=self.font_weight.get())

    def configureWinSize(self):
        self.root.geometry(f'{self.window_width.get()}x{self.window_height.get()}+{self.window_right_offset.get()}+{self.window_top_offset.get()}')

    def __init__(self, root):
        self.root = root
        self.root.title('UpTimeMonitor')

        self.root.protocol("WM_DELETE_WINDOW", self._exit)

        self.setUpSettingsVariables()
        self.setUpVariables()
        self.setUpMenu()

        self.main_font = tkf.nametofont("TkDefaultFont")
        self.configureMainFont()
        self.heading_font = tkf.Font(family="Helvetica", size=15)

        self.root.option_add("*Font", self.main_font)

        self.configureWinSize()

        self.display_main_win()

    def _exit(self):
        if messagebox.askyesno("Zavřít", "Opravdu chcete odejít?"):
            self.root.quit()


if __name__ == "__main__":
    root = Tk()
    app = App(root)
    root.mainloop()
