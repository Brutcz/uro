# http://www.haucks.org/

from tkinter import *
from collections import defaultdict
import tkinter.constants as tkc


class Observable(object):

    def __init__(self):
        self.__items = {}

    def emit(self, *args):
        '''Pass parameters to all observers and update states.'''
        for subscriber in self.__items:
            response = subscriber(*args)
            self.__items[subscriber] = response

    def subscribe(self, subscriber):
        '''Add a new subscriber to self.'''
        self.__items[subscriber] = None

    def stat(self):
        '''Return a tuple containing the state of each observer.'''
        return tuple(self.__items.values())


class MultiListbox(Frame, Observable):
    def __init__(self, master, lists):
        Frame.__init__(self, master)
        Observable.__init__(self)
        self.lists = []
        for l, w in lists:
            frame = Frame(self)
            frame.pack(side=tkc.LEFT, expand=tkc.YES, fill=tkc.BOTH)
            Label(frame, text=l, borderwidth=1, relief=tkc.RAISED).pack(fill=tkc.X)
            lb = Listbox(frame, width=w, borderwidth=0, selectborderwidth=0, relief=tkc.FLAT, exportselection=tkc.FALSE)
            lb.pack(expand=tkc.YES, fill=tkc.BOTH)
            self.lists.append(lb)
            lb.bind('<B1-Motion>', lambda e, s=self: s._select(e.y))
            lb.bind('<Button-1>', lambda e, s=self: s._select(e.y))
            lb.bind('<Leave>', lambda e: 'break')
            lb.bind('<B2-Motion>', lambda e, s=self: s._b2motion(e.x, e.y))
            lb.bind('<Button-2>', lambda e, s=self: s._button2(e.x, e.y))
        frame = Frame(self)
        frame.pack(side=tkc.LEFT, fill=tkc.BOTH)
        Label(frame, borderwidth=1, relief=tkc.RAISED).pack(fill=tkc.X)
        sb = Scrollbar(frame, orient=tkc.VERTICAL, command=self._scroll)
        sb.pack(expand=tkc.YES, fill=tkc.Y)
        self.lists[0]['yscrollcommand'] = sb.set

    def select_default(self):
        self.selection_clear(0, tkc.END)
        self.selection_set(0)
        self.emit(0)

    def _select(self, y):
        row = self.lists[0].nearest(y)
        self.selection_clear(0, tkc.END)
        self.selection_set(row)
        self.emit(row)
        return 'break'

    def _button2(self, x, y):
        for l in self.lists:
            l.scan_mark(x, y)
        return 'break'

    def _b2motion(self, x, y):
        for l in self.lists:
            l.scan_dragto(x, y)
        return 'break'

    def _scroll(self, *args):
        for l in self.lists:
            l.yview(*args)

    def curselection(self):
        return self.lists[0].curselection()

    def delete(self, first, last=None):
        for l in self.lists:
            l.delete(first, last)

    def get(self, first, last=None):
        result = []
        for l in self.lists:
            result.append(l.get(first, last))
        if last:
            return result
        return result

    def update(self, index, *elements):
        self.delete(index)
        self.insert(index, *elements)
        self.selection_set(index)

    def index(self, index):
        self.lists[0].index(index)

    def insert(self, index, *elements):
        for e in elements:
            i = 0
            for l in self.lists:
                l.insert(index, e[i])
                i = i + 1

    def size(self):
        return self.lists[0].size()

    def see(self, index):
        for l in self.lists:
            l.see(index)

    def selection_anchor(self, index):
        for l in self.lists:
            l.selection_anchor(index)

    def selection_clear(self, first, last=None):
        for l in self.lists:
            l.selection_clear(first, last)

    def selection_includes(self, index):
        return self.lists[0].selection_includes(index)

    def selection_set(self, first, last=None):
        for l in self.lists:
            l.selection_set(first, last)
