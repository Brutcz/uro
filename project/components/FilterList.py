from tkinter import *
from tkinter.ttk import *
from .MultiListbox import MultiListbox as Table
import tkinter.constants as tkc


class FilterList(Frame):
    def OnEntryChange(self, event):
        searched_val = self.search_text.get()
        find_in_text = any(map(lambda x: x.find(searched_val) != -1, self.listbox.get(0, tkc.END)[0]))
        if len(searched_val) > 0 and find_in_text:
            self.add_btn.pack_forget()
        elif len(searched_val) != 0:
            self.add_btn.pack()
        else:
            self.add_btn.pack_forget()

    def get_selected_row(self, index):
        return self.listbox.get(index)

    def curselection(self):
        return self.listbox.curselection()

    def update(self, index, *elements):
        return self.listbox.update(index, *elements)

    def add2list(self):
        self.data[self.search_text.get()] = {}
        self.data[self.search_text.get()][self.def_key] = self.def_val

        self.listbox.insert(tkc.END, (self.search_text.get(), 'ok'))

    def subscribe(self, func):
        self.listbox.subscribe(func)

    def select_default(self):
        self.listbox.select_default()

    def __init__(self, parent, *args, **kw):
        Frame.__init__(self, parent)

        self.search_text = StringVar()

        self.controll_top_frame = Frame(self)

        self.search = Entry(self.controll_top_frame, textvariable=self.search_text)
        self.search.bind("<KeyRelease>", self.OnEntryChange)  # keyup
        self.search.pack(expand=1, side=tkc.LEFT, fill=tkc.X)

        self.add_btn = Button(self.controll_top_frame, text='+', command=self.add2list)
        # self.add_btn.pack(expand=1, side=LEFT)

        self.controll_top_frame.pack(expand=1, fill=tkc.X)

        self.listbox_frame = Frame(self)

        self.listbox = Table(self.listbox_frame, kw['dataset'])
        self.listbox.pack(expand=1, side=tkc.LEFT, fill=tkc.BOTH)

        self.listbox_frame.pack(expand=1, fill=tkc.BOTH)

        self.def_key = kw['def_key'] if 'def_key' in kw else None

        self.def_val = kw['def_val'] if 'def_val' in kw else ''

        if 'data' in kw:
            self.data = kw['data']

            for key, val in kw['data'].items():
                if not self.def_key:
                    self.def_key = list(val.keys())[0]

                self.listbox.insert(tkc.END, (key, val[self.def_key]))
