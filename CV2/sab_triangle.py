# -*- coding: utf-8 -*-

from tkinter import *
from math import sqrt

class myApp:

    def vymaz(self):
        self.enta.delete(0, END)
        self.enta.insert(0, '0')
        
        self.entb.delete(0, END)
        self.entb.insert(0, '0')
        
        self.entc.delete(0, END)
        self.entc.insert(0, '0')

        self.enta.focus_force()

    def obvod(self):
        return self.a+self.b+self.c

    def obsah(self):
        s = self.obvod() / 2
        return sqrt(s*(s-self.a)*(s-self.b)*(s-self.c))

    def je_trojuhelnik(self):
        return (self.a < (self.b + self.c)) and (self.b < (self.a + self.c)) and (self.c < (self.a + self.b))
        #return False

    def je_pravouhly(self):
        return (self.a**2 + self.b**2 == self.c**2 or self.a**2 + self.c**2 == self.b**2 or self.b**2 + self.c**2 == self.a**2)
        #return False

    def je_rovnostranny(self):
        return self.a == self.b == self.c
        #return False

    def vyresit(self):
        try:
            self.a = float(self.enta.get())
            self.b = float(self.entb.get())
            self.c = float(self.entc.get())

            txt = "Obecný trojúhelník"

            if self.je_trojuhelnik():
                if self.je_pravouhly(): txt = "Pravoúhlý trojúhelník"
                if self.je_rovnostranny(): txt = "Rovnostranný trojúhelník"
                # vypsat vysledek
                self.result.configure(text="{0}\n\nObvod = {1:.2f}\nObsah = {2:.2f}".format(txt, self.obvod(), self.obsah()), fg='Blue')
            else:
                self.result.configure(text="Toto není trojúhelník", fg='Red')
               # vypsat, ze neni trojuhelnik

        except ValueError:
            # vypsat, ze strany nebyly spravne zadany
            self.result.configure(text="Strany byly zadány špatně", fg='Red')

    def __init__(self, root):

        root.title('Triangle')

        self.top = Frame(root)
        self.top.pack(expand=1, fill=BOTH)

        #nadpis
        self.titlef = Frame(self.top, relief=GROOVE, borderwidth=2)
        self.titlef.pack(expand=1, fill=X, side=TOP, padx=4, pady=4, ipadx=4, ipady=4)

        self.titleL = Label(self.titlef, text="Obvod a obsah trojúhelníku")
        self.titleL.pack(expand=1, fill=BOTH, pady=4)

        # zadani
        self.zadanif = Frame(self.top, relief=GROOVE, borderwidth=2)
        self.zadanif.pack(expand=1, fill=Y, side=LEFT, padx=4, pady=4, ipady=4)

        input_pads = {
            'padx': 8,
            'pady': 3,
        }

        input_w = 14
        
        input_l_pads = {
            'padx': 8,
            'pady': 1,
        }

        self.la=Label(self.zadanif, text="Strana a =")
        self.la.pack(expand=1, fill=X, **input_l_pads)
        self.enta = Entry(self.zadanif, width = input_w)
        self.enta.pack(expand=1, **input_pads)

        self.lb=Label(self.zadanif, text="Strana b =")
        self.lb.pack(expand=1, fill=X, **input_l_pads)
        self.entb = Entry(self.zadanif, width=input_w)
        self.entb.pack(expand=1, **input_pads)

        self.lc=Label(self.zadanif, text="Strana c =")
        self.lc.pack(expand=1, fill=X, **input_l_pads)
        self.entc = Entry(self.zadanif, width=input_w)
        self.entc.pack(expand=1, **input_pads)

        self.but = Button(self.zadanif, text='Vymazat', command=self.vymaz)
        self.but.pack(expand=1, padx=4, pady=4)

        self.vymaz()
        self.enta.focus_force()

        #vysledek
        self.resultf = Frame(self.top, relief=GROOVE, borderwidth=2)
        self.resultf.pack(expand=1, fill=X, side=TOP, padx=4, pady=4, ipady=4)

        self.resultL = Label(self.resultf, text="Výsledek")
        self.resultL.pack(expand=1, fill=X, pady=4)
        self.result = Label(self.resultf, text='', bg='Grey', fg='Red')
        self.result.pack(expand=1, fill=BOTH, ipady=50, ipadx=50, pady=10, padx=10)

        #buttons

        btn_pads = {
            'pady': 5,
            'padx': 5,
            'ipady': 10,
            'ipadx': 15,
            
        }

        self.solveb = Button(self.top, text='Vyřeš', command=self.vyresit, relief=RAISED, borderwidth=2)
        self.solveb.pack(expand=1, side=LEFT, **btn_pads)
        
        self.endb = Button(self.top, text='Konec', command=root.destroy, relief=RAISED, borderwidth=2)
        self.endb.pack(expand=1, side=LEFT, **btn_pads)

root = Tk()
app = myApp(root)
root.mainloop()

