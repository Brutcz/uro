#------------------------------------------------------------------------------#
# Kalkulacka                                                                   #
#------------------------------------------------------------------------------#

from tkinter import * 
from math import *
from tkinter import font

def callback(f, *a):
    """
    Vraci funkci f s nastavenym parametrem a.
    Tato funkce umi osetrit i vice nez jeden parametr.

    Pouziti:
        callback(funkce, arg1, arg2, ..., argn )
    """
    return lambda: f(*a)

class MyApp:

    def big(self):
        print("big")
        self.font.configure(weight='bold', size="25")

    def normal(self):
        self.font.configure(weight='normal', size="10")
        print("normal")

    def insKey(self, znak):

        if znak == '=':
            print("History: {0}={1:.2f}".format(self.boo.get(), eval(self.boo.get())))
            self.boo.set("{0:.2f}".format(eval(self.boo.get())))
            return
        if znak == 'cls':
            self.la.configure(text='0')
            self.boo.set('0')
            return

        #print("insKey:", znak)
        
        if self.boo.get() == '0': self.boo.set(znak)
        else: self.boo.set(self.boo.get() + znak)
        
    def __init__(self, root):
        self.fo = StringVar()
        root.title("Calculator")
        self.font = font.Font(size=10, weight="normal")

        self.boo = StringVar()
        self.boo.set('0')

        self.la = Label(root, textvariable=self.boo, background="#ffffff", anchor=E, relief=SUNKEN, height=2, font=self.font)
        self.la.pack(fill=X, side=TOP, padx=8, pady=5)

        self.opts = Frame(root, relief=GROOVE)
        self.opts.pack()

        self.nor = Radiobutton(self.opts, text="Normal", variable=self.fo, value="normal", command=self.normal, font=self.font)
        self.nor.pack(side=LEFT)
        
        self.bir = Radiobutton(self.opts, text="Big", variable=self.fo, value="big", command=self.big, font=self.font)
        self.bir.pack(side=LEFT)

        # Calc buttons frame
        self.numbts = Frame(root)
        self.numbts.pack(side=TOP, fill=BOTH, expand=1, padx=4, pady=4)
        
        self.numbts.rowconfigure(0, weight=1)
        self.numbts.rowconfigure(1, weight=1)
        self.numbts.rowconfigure(2, weight=1)
        self.numbts.rowconfigure(3, weight=1)
        self.numbts.rowconfigure(4, weight=1)
        self.numbts.rowconfigure(5, weight=1)
        self.numbts.columnconfigure(0, weight=1)
        self.numbts.columnconfigure(1, weight=1)
        self.numbts.columnconfigure(2, weight=1)
        self.numbts.columnconfigure(3, weight=1)

        btn_conf = {
            'sticky': W+E+N+S,
            'padx': 2,
            'pady': 2,
        }

        # Calc buttons
        self.btn_cls = Button(self.numbts, text="Cls",width=5, height=2, font=self.font, command=callback(self.insKey, "cls"))
        self.btn_cls.grid(row=1, column=0,**btn_conf)
        
        self.btn_div = Button(self.numbts, text="/",width=5, height=2, font=self.font, command=callback(self.insKey, "/"))
        self.btn_div.grid(row=1, column=1,**btn_conf)
        
        self.btn_mul = Button(self.numbts, text="*",width=5, height=2, font=self.font, command=callback(self.insKey, "*"))
        self.btn_mul.grid(row=1, column=2,**btn_conf)
        
        self.btn_m = Button(self.numbts, text="-",width=5, height=2, font=self.font, command=callback(self.insKey, "-"))
        self.btn_m.grid(row=1, column=3,**btn_conf)
        
        self.btn_9 = Button(self.numbts, text="9",width=5, height=2, font=self.font, command=callback(self.insKey, "9"))
        self.btn_9.grid(row=2, column=0,**btn_conf)
        
        self.btn_8 = Button(self.numbts, text="8",width=5, height=2, font=self.font, command=callback(self.insKey, "8"))
        self.btn_8.grid(row=2, column=1,**btn_conf)
        
        self.btn_7 = Button(self.numbts, text="7",width=5, height=2, font=self.font, command=callback(self.insKey, "7"))
        self.btn_7.grid(row=2, column=2,**btn_conf)
        
        self.btn_p = Button(self.numbts, text="+",width=5, height=2, font=self.font, command=callback(self.insKey, "+"))
        self.btn_p.grid(row=2, column=3, rowspan=2,**btn_conf)
        
        self.btn_6 = Button(self.numbts, text="6",width=5, height=2, font=self.font, command=callback(self.insKey, "6"))
        self.btn_6.grid(row=3, column=0,**btn_conf)
        
        self.btn_5 = Button(self.numbts, text="5",width=5, height=2, font=self.font, command=callback(self.insKey, "5"))
        self.btn_5.grid(row=3, column=1,**btn_conf)
        
        self.btn_4 = Button(self.numbts, text="4",width=5, height=2, font=self.font, command=callback(self.insKey, "4"))
        self.btn_4.grid(row=3, column=2,**btn_conf)

        self.btn_res = Button(self.numbts, text="=",width=5, height=2, font=self.font, command=callback(self.insKey, "="))
        self.btn_res.grid(row=4, column=3, rowspan=2,**btn_conf)
        
        self.btn_3 = Button(self.numbts, text="3",width=5, height=2, font=self.font, command=callback(self.insKey, "3"))
        self.btn_3.grid(row=4, column=0,**btn_conf)
        
        self.btn_2 = Button(self.numbts, text="2",width=5, height=2, font=self.font, command=callback(self.insKey, "2"))
        self.btn_2.grid(row=4, column=1,**btn_conf)
        
        self.btn_1 = Button(self.numbts, text="1",width=5, height=2, font=self.font, command=callback(self.insKey, "1"))
        self.btn_1.grid(row=4, column=2,**btn_conf)
        
        self.btn_0 = Button(self.numbts, text="0",width=5, height=2, font=self.font, command=callback(self.insKey, "0"))
        self.btn_0.grid(row=5, column=0, columnspan=2,**btn_conf)
        
        self.btn_dot = Button(self.numbts, text=",",width=5, height=2, font=self.font, command=callback(self.insKey, ","))
        self.btn_dot.grid(row=5, column=2,**btn_conf)

        self.nor.select()

        # Greeting text frame
        self.greetingf = Frame(root, relief=GROOVE)
        self.greetingf.pack()

        # Greeting text
        self.greetingl = Label(self.greetingf, text="Uživatelská rozhraní", fg='Blue')
        self.greetingl.pack(fill=BOTH)

root = Tk()
app = MyApp(root)
root.mainloop()
