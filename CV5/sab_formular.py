# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import os

import MultiListbox as table

data = [
        ["Petr", "Bílý","045214/1512", "17. Listopadu", 15, "Ostrava", 70800,"poznamka"],
        ["Jana", "Zelený","901121/7238", "Vozovna", 54, "Poruba", 78511,""],
        ["Karel", "Modrý","800524/5417", "Porubská", 7, "Praha", 11150,""],
        ["Martin", "Stříbrný","790407/3652", "Sokolovská", 247, "Brno", 54788,"nic"]
       ]

BASE_DIR = os.path.dirname(__file__)

class App(object):
    def __init__(self, master):
        self.master = master
        self.row = IntVar()
        self.jmeno = StringVar()
        self.prijmeni = StringVar()
        self.rc = StringVar()
        self.ulice = StringVar()
        self.cp = StringVar()
        self.mesto = StringVar()
        self.psc = StringVar()
        self.note = StringVar()

        self.attrs = [ self.jmeno, self.prijmeni, self.rc, self.ulice, self.cp, self.mesto, self.psc, self.note ]

        self.mlb = table.MultiListbox(master, (('Jméno', 20), ('Příjmení', 20), ('Rodné číslo', 12)))
        for i in range(len(data)):
            self.mlb.insert(END, (data[i][0], data[i][1],data[i][2]))
        self.mlb.pack(expand=YES,fill=BOTH, padx=10, pady=10)
        self.mlb.subscribe( lambda row: self.edit( row ) )

        sticky = {
            'sticky': (W, E)
        }

        self.pdf = Frame(master)
        self.pdf.pack(expand=YES, fill=BOTH)
        self.jmenol = Label(self.pdf, text='Jméno')
        self.jmenol.grid(row=0, column=1, **sticky)
        self.jmenoe = Entry(self.pdf, textvariable=self.jmeno)
        self.jmenoe.grid(row=0, column=2, sticky=(W))
        self.prijmenil = Label(self.pdf, text='Příjmení')
        self.prijmenil.grid(row=1, column=1, **sticky)
        self.prijmenie = Entry(self.pdf, textvariable=self.prijmeni)
        self.prijmenie.grid(row=1, column=2, **sticky)
        self.rcl = Label(self.pdf, text='Rodné číslo')
        self.rcl.grid(row=2, column=1, **sticky)
        self.rce = Entry(self.pdf, textvariable=self.rc)
        self.rce.grid(row=2, column=2, **sticky)

        self.nb = ttk.Notebook(master)
        self.p1 = LabelFrame(self.nb, text='Adresa')
        self.p2 = LabelFrame(self.nb, text='Poznámka')
        self.nb.add(self.p1, text="Adresa")
        self.nb.add(self.p2, text="Poznámka")
        self.nb.pack(expand=1, fill=BOTH)
        #p1

        self.ulicel = Label(self.p1, text="Ulice")
        self.ulicel.pack()
        self.ulicee = Entry(self.p1, textvariable=self.ulice)
        self.ulicee.pack()
        self.cpl = Label(self.p1, text="Č.p.")
        self.cpl.pack()
        self.cpe = Entry(self.p1, textvariable=self.cp)
        self.cpe.pack()
        self.mestol = Label(self.p1, text="Město")
        self.mestol.pack()
        self.mestoe = Entry(self.p1, textvariable=self.mesto)
        self.mestoe.pack()
        self.pscl = Label(self.p1, text="PSČ")
        self.pscl.pack()
        self.psce = Entry(self.p1, textvariable=self.psc)
        self.psce.pack()
        #p2
        self.notee = Entry(self.p2, textvariable=self.note)
        self.notee.pack()

        #action button
        self.cancelbtn = Button(master, text="Zavřít", command=self.cancel)
        self.cancelbtn.pack()

        self.new_recerd_btn = Button(master, text="Nový záznam", command=self.new)
        self.new_recerd_btn.pack()

        self.save_record_btn = Button(master, text="Uložit záznam", command=self.save)
        self.save_record_btn.pack()
        
        self.profile_btn = Button(master, text='Profil', command=self.profileWin, state=DISABLED)
        self.profile_btn.pack()

    def new(self):
        self.profile_btn.config(state=DISABLED)
        self.row = -1

        for i, attr in enumerate(self.attrs):
            attr.set('')

    def save(self):
        val = []
        for attr in self.attrs:
            val.append(attr.get())

        if self.row != -1:
            data[self.row] = val
        else:
            data.append(val)

        print(val)
        self.profile_btn.config(state=NORMAL)

    def cancel(self):
        if messagebox.askyesno("Zavřít", "Opravdu?"):
            self.master.quit()
      
    def edit(self, row):
        self.profile_btn.config(state=NORMAL)
        self.row = row
        #print (data[row])

        for i, attr in enumerate(self.attrs):
            attr.set(data[row][i])
        
        # self.jmeno.set(data[row][0])
        # self.prijmeni.set(data[row][1])
        # self.rc.set(data[row][2])
        # self.ulice.set(data[row][3])
        # self.cp.set(data[row][4])
        # self.mesto.set(data[row][5])
        # self.psc.set(data[row][6])
        # self.note.set(data[row][7])

    def profileWin(self):
        win = Toplevel()
        lbf_info = LabelFrame(win, text="Informace")
        lbf_info.pack(expand=1, fill=BOTH)

        photo = PhotoImage(file=BASE_DIR + "/img.png")
        canvas_photo = Canvas(lbf_info, width=100, height=100)
        canvas_photo.create_image(50, 50, image=photo)
        canvas_photo.image = photo
        canvas_photo.pack(side=LEFT)
        

        
        lbf_map = LabelFrame(win, text="Mapa")
        lbf_map.pack(expand=1, fill=BOTH)
        # canvas_map = Canvas(lbf_map, width=100, height=100)
        # canvas_map.pack(expand=1)

        # slider = Scale(lbf_map, from_=0, to=100, orient=HORIZONTAL, showvalue=0, command=canvas_map.xview)


        mapa = PhotoImage(file=BASE_DIR + "/mapa.png")
        canvas_map=Canvas(lbf_map, bg='#000', width=100, height=100, scrollregion=(0,0,500,500))
        canvas_map.create_image(50, 50, image=mapa)
        canvas_map.image = mapa
        hbar=Scrollbar(lbf_map, orient=HORIZONTAL)
        hbar.pack(side=BOTTOM,fill=X)
        hbar.config(command=canvas_map.xview)
        vbar=Scrollbar(lbf_map, orient=VERTICAL)
        vbar.pack(side=RIGHT,fill=Y)
        vbar.config(command=canvas_map.yview)
        canvas_map.config(width=100,height=100)
        canvas_map.config(xscrollcommand=hbar.set, yscrollcommand=vbar.set)
        canvas_map.pack(side=LEFT,expand=True,fill=BOTH)

        #udalosti preneseny na nove okno - stare neaktivni
        win.grab_set()
        #svaze okno s rodicem - nezobrazi se v liste
        # root.transient(self.master)
             

root = Tk()
root.wm_title("Formulář")
app = App(root)
root.mainloop()
